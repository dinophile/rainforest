#Rainforest

##Introduction

Amazon-like clone rails assignment project, where we learned the steps to creating a rails project start to finish. It incorporated ajax and JSON, as well as focused on authentication hashing, model and association building.

##Deployment

This is a simple web project, deployment can be on any web server or local file system.

