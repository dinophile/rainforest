class Review < ActiveRecord::Base
	belongs_to :user
	belongs_to :product

	validates :comment, presence: true
	# validate :blank_review
	#
	# def blank_review
	# 	if comment.blank?
	# 		errors.add[:comment, "You cannot submit a blank review!"]
	# 	end
	# end
end
