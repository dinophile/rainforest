class ChangePasswordDigest < ActiveRecord::Migration
	def change
		rename_column :users, :password_digest_, :password_digest
	end
end
