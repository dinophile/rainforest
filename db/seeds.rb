# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Product.destroy_all
User.destroy_all
Review.destroy_all

cat_herder = Product.create!(
	:name => "Cat Herding Kit",
	:description => "The finest cat herding kit money can buy!",
	:price_in_cents => 1000
)

motion = Product.create!(
	:name => "Perpetual Motion Machine",
	:description => "Not your run-of-the-mill machine!",
	:price_in_cents => 10000
)

cheyenne = User.create!(
	:name => "Cheyenne",
	:email => "koshismycat@gmail.com",
	:password => "password"
)

Review.create!(
	:comment => "This product is awesome!",
	:product_id => cat_herder.id, 
	:user_id => cheyenne.id
)
