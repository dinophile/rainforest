FactoryGirl.define do
	factory :product do
		name "Cat Herding Kit"
		description "The ultimate in cat herding technology."
		price_in_cents 4900000
	end
end
